export const api_url = 'https://jsonplaceholder.typicode.com/'
export const HEADER  = {
    headers: {"Access-Control-Allow-Origin": "*"}
}
export const UTF     = {
    'content-type': 'application/x-www-form-urlencoded;charset=utf-8'
}

export const storage = {
    get : (key) => {
        return window.localStorage.getItem(key)
    },
    set: (key, value) => {
        return window.localStorage.setItem(key,value)
    },
    remove: (key) => {
        window.localStorage.removeItem(key);
    }
}
