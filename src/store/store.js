import {applyMiddleware, combineReducers, createStore} from "redux";
import {statInfoReducer,statInfosById} from "./reducers";
import { composeWithDevTools } from 'redux-devtools-extension';
import thunk from 'redux-thunk';
const middleware = [thunk];

const reducers = combineReducers({
    users:statInfoReducer,
    byId:statInfosById
});

export default createStore(reducers,composeWithDevTools(applyMiddleware(...middleware)));