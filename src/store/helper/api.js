import { api_url,HEADER} from "./helper"
import axios from 'axios'

class API {
    constructor() {
        this.setCustomUrl = function (url) {
            return api_url  + url;
        }
    }
    getUsers(){
        return axios.get(
                this.setCustomUrl(`users`),
                {headers: HEADER}
                );
    }
    getUsersById(id){
        return axios.get(
                this.setCustomUrl(`users/${id}`),
                {headers: HEADER}
                );
    }
}

const api = new API();

export default api;