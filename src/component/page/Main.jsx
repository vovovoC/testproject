import {Component} from 'react'
import { connect} from 'react-redux'
import Table from './Table'
import {getUsersList} from '../../store/actions'
class Main extends Component{
    constructor(props){
        super(props)
        this.state={
            data:null
        }
    }
    componentDidMount(){
        this.props.getUserList().then((res)=>{
            this.setState({data : res})
        })    
    }   
    render(){
        return(
            <div>
                {
                  this.state.data? <Table data={this.state.data}/>:null
                }
            </div>
        )
    }
}
const mapStateToProps = state => ({ users:state.statInfos})
const mapDispatchToProps = dispatch => {
    return {
       getUserList :() => dispatch(getUsersList())
    };
  };
export default connect(mapStateToProps, mapDispatchToProps)(Main)