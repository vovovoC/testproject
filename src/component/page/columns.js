export const COLUMNS = [
    {
      Header: 'Id',
      accessor: 'id',
      disableFilters: true,
      sticky: 'left'
    },
    {
      Header: 'User Name',
      accessor: 'username',
      sticky: 'left'
    },
    {
      Header: 'Email',
      accessor: 'email',
      sticky: 'left'
    },
    {
      Header: 'Website',
      accessor: 'website'
    },
  ]