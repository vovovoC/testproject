import {constants} from "./constants"
import api from "./helper/api"
export const getUsersList = () => async (dispatch) =>{
    try{
        const res = await api.getUsers();
        dispatch({
            type: constants.TABLE_INFO,
            payload: res.data
        })
        return Promise.resolve(res.data)
    }
    catch (err){
        return Promise.reject(err)
    }
}
export const getUserById = () => async (dispatch) =>{
    try{
        const res = await api.getUserById()
        dispatch({
            type: constants.TABLE_INFO,
            payload: res.data
        });
        return Promise.resolve(res.data)
    }
    catch (err){
        return Promise.reject(err)
    }
}