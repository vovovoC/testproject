import style from 'styled-components'
export const Container = style.div`
    height:auto;
    width:90%;
    margin:30px 5%;
    table {
        width: 100%;
      }
      table td,
      table th {
        border: 1px solid #ddd;
        padding: 8px;
      }
      table tr:nth-child(even) {
        background-color: #f2f2f2;
      }
      
      table tr:hover {
        background-color: #ddd;
      }
      
      table th, tfoot td {
        padding-top: 12px;
        padding-bottom: 12px;
        background-color:#004bc4;
        color: white;
      }
      button{
          cursor:pointer;
      }
`