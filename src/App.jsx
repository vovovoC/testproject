import {BrowserRouter as Router,Route, Switch } from 'react-router-dom'
import Main from './component/page/Main'
import User from './component/page/User'

function App() {
  return (
      <Router>
      <Switch>
        <Route path="/" exact component={Main}/>
        <Route path="/users/:id" component={User}/>
      </Switch>
      </Router>

  );
}

export default App;