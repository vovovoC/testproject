import {statInfos,infoById} from './states'
import {constants} from './constants'

// statistics page

export const statInfoReducer = (state= statInfos,{type,payload}) => {
    switch (type) {
        case constants.TABLE_INFO:
            return {...state, data: {...payload}};
        default:
            return {...state}
    }
}
export const statInfosById = (state= infoById,{type,payload}) => {
    switch (type) {
        case constants.USER_ID:
            return {...state, data: {...payload}};
        default:
            return {...state}
    }
}