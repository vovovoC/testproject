import {useTable,usePagination} from 'react-table'
import React,{useMemo,useState} from 'react'
import {useHistory} from 'react-router-dom'
import {COLUMNS} from './columns'
import {Container} from '../global/Styled'

export default function Table({ data}) {
    const columns = useMemo(()=>COLUMNS,[])
    const [stateInfo,setStateInfo] = useState(false)
    const [checkedUsers,setCheckedUsers] = useState([])
    const [mainInfo,setMainInfo] = useState(data)
    var info = useMemo(()=> mainInfo,[stateInfo])
    const history = useHistory()
    const tableInstance = useTable({
      columns:columns,
      data:info,
    },
    usePagination
    )
    const {
      getTableProps,
      getTableBodyProps,
      headerGroups,
      canPreviousPage,
      canNextPage,
      nextPage,
      previousPage,
      page,
      prepareRow,
      setPageSize,
    }  = tableInstance

    const handleCheck = (value)=>{
     if(checkedUsers.includes(value)){
       if(checkedUsers.length<2){
         alert('Please, choose at least one item!')
       }
      else{
        var arr = checkedUsers.filter(e => e != value)
        setCheckedUsers(arr)
      }
     }
     else{
       checkedUsers.push(value)
       alert(checkedUsers)
     }
    }
   const removeUser=(value)=>{
     setStateInfo(!stateInfo)
     var arr = info.filter(e => e.id !== value)
     setMainInfo(arr)
   }
    return (
    <>
    { data && data.length > 0 ? 
      <Container>
    <table {...getTableProps()}>
        <thead>
       {
         headerGroups.map(headerGroup=>(
          <tr {...headerGroup.getHeaderGroupProps()}>
            <th></th>
            {
              headerGroup.headers.map(column=>(
                 <th {...column.getHeaderProps()}>{column.render('Header')}</th>
              ))
            }
           <th></th>
           <th></th>
          </tr>
         ))
       }
        </thead>
        <tbody {...getTableBodyProps()}>
          {
            page.map(row=>{
              prepareRow(row)
              return(
                <tr {...row.getRowProps()}>
                  <td>
                    <input type="radio" checked={checkedUsers.includes(row.cells[0].value)} onClick={()=>(handleCheck(row.cells[0].value))}/>
                  </td>
                  {
                    row.cells.map(cell=>{
                    return <td {...cell.getCellProps()}>{cell.render('Cell')}</td>
                    })
                  }
                  <td><button onClick={()=>(history.push(`/users/${row.cells[0].value}`))}>Подробнее</button></td>
                  <td><button onClick={()=>(removeUser(row.cells[0].value))}>Удалить</button></td>
                </tr>
              )
            })
          }
        </tbody>
      </table> 
      <select onChange={(e)=>(setPageSize(Number(e.target.value)))}>
         {
           [10,5,2].map(pageSize=>(
             <option key={pageSize} value={pageSize}>{pageSize}</option>
           ))
         }
      </select>
      <div>
        <button onClick={()=>(previousPage())} disabled={!canPreviousPage}>Prev</button>
        <button onClick={()=>(nextPage())} disabled={!canNextPage}>Next</button>
      </div>
      </Container>
      :null}
       </>
    )
  }